import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def read_data(binsize,hashid):
    folder = 'data/C2A2_data/BinnedCsvs_d{}/'.format(binsize)
    file = folder + hashid +'.csv'
    df = pd.read_csv(file)
    return df

def fix_dates(df):
    df['Date'] = pd.to_datetime(df['Date'], format='%Y-%m-%d')
    df = df.reset_index()
    df = df.sort_values(by=['Date']).drop(['index', 'ID'], axis=1)
    df['Data_Value'] = df['Data_Value']/10
    return df

binsize = 400
hashid = 'fb441e62df2d58994928907a91895ec62c2c42e6cd075c2700843b89'

temp = read_data(binsize, hashid)
temp2 = fix_dates(temp)

def my_agg(x):
    names = {
       'TMIN': x[x['Element'] == 'TMIN']['Data_Value'].min(),   
       'TMAX': x[x['Element'] == 'TMAX']['Data_Value'].max()}

    return pd.Series(names, index=['TMIN','TMAX'])

def split_year(df, year):
    t_year = df[df['Date'].dt.year==year]
    t_rest = df[df['Date'].dt.year!=year]
    return t_year,t_rest
    

def agg_temp(df):
    df = df.groupby(by= ['Date']).apply(my_agg).reset_index()
    df = df.groupby([(df['Date'].dt.month),(df['Date'].dt.day)]).agg({ 'TMAX':'max',
                                                                               'TMIN':'min'
                                                                          })
    df.index.names=['Month', 'Day']
    
    # Drop leap years
    if (2,29) in df.index:
        return df.drop((2,29))
    else:
        return df
    

t15, trest= split_year(temp2,2015)
t15 = agg_temp(t15)
trest = agg_temp(trest)


x = np.arange(0,365)
def record_breakers(x,df,data):
    if data == 'TMIN':
        x_r = x[df[data] < trest[data]]-0.1
        t_r = df[df[data] < trest[data]][data]-0.1
        return x_r,t_r
    elif data == 'TMAX':
        x_r = x[df[data] > trest[data]]+0.1
        t_r = df[df[data] > trest[data]][data]+0.1
        return x_r,t_r
    
x_min, t_min = record_breakers(x,t15,'TMIN') 
x_max, t_max = record_breakers(x,t15,'TMAX')



ax = plt.gca()

month_starts = [1,32,61,92,122,153,183,214,245,275,306,336]
month_names = ['Jan','Feb','Mar','Apr','May','Jun',
               'Jul','Aug','Sep','Oct','Nov','Dec'] 
locs = [(month_starts[i]-month_starts[i-1])/2+month_starts[i-1] for i in range(1,len(month_starts))]

ax.xaxis.set_ticks(month_starts)
ax.xaxis.set_ticklabels(month_names)
plt.xticks( rotation=45)
ax = plt.gca()
ax.axis([0,365,-40,45])
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Temperature [C]')
plt.title('Yearly record temperature highs and lows (2004-2014)')
color = 'green'
offs = 0.4
plt.plot(list(trest['TMAX']), label='_nolegend_', color=color, alpha=0.8-offs)
plt.plot(list(trest['TMIN']), label='_nolegend_', color=color, alpha=0.8-offs)
plt.fill_between(x,trest['TMAX'], trest['TMIN'],color=color, alpha= 0.5-offs)
plt.scatter(x_min, t_min,color='blue', label='2015 Record high', alpha=0.7 )
plt.scatter(x_max, t_max,color='red', label='2015 Record low', alpha=0.7)

plt.legend(loc=4)

# plt.show()
plt.savefig('figname.png', transparent=True)

