# %matplotlib notebook
import matplotlib.pyplot as plt
from scipy.stats import norm
from matplotlib import cm
from matplotlib.colors import ListedColormap
from matplotlib.cm import ScalarMappable
from matplotlib.widgets import Slider, Button, RangeSlider
import numpy as np
np.set_printoptions(precision=6)

data = df.T.describe()
mean = data.loc['mean']
sigma = data.loc['std']
l = df.shape[1]
Z = 1.960
error = sigma/np.sqrt(l)*Z

y_max = 40000
y_min = 30000
dists = [1- norm(loc=mean.iloc[i], scale=sigma.iloc[i]/np.sqrt(l)).cdf(y_val) for i in range(4) ]

def find_dists(y_val):
    dists = [1- norm(loc=mean.iloc[i], scale=sigma.iloc[i]/np.sqrt(l)).cdf(y_val) for i in range(4) ]
    print(dists)
    return np.array(dists)

def diff_dists(x):
    return (-find_dists(x[1]) + find_dists(x[0]))
bwr = cm.get_cmap('jet', 256)
sc_map = ScalarMappable(cmap=bwr)
sc_map.set_array([])
colors = bwr(dists)

def plot_hline(y_val):
    x_list = np.arange(1991,1997,1)
    y_list = np.ones_like(x_list)*y_val
    ax.plot(x_list,y_list, color='red')

fig, (sax, ax, cax, ) = plt.subplots(ncols=3,figsize=(6,6), 
                  gridspec_kw={"width_ratios":[0.05, 1,0.05]})

ax.bar(data.columns, data.loc['mean'], yerr=error, align='center', alpha=1, ecolor='black', capsize=10, color= colors)
ax.set_xlim(1991, 1996)

y_slider = RangeSlider(
    ax=sax,
    label='Y ranges',
    valmin=0,
    valmax=60000,
    valinit=(30000,40000),
    orientation='vertical'
)
sax.sharey(ax)
def update(val):
    ax.cla()
    for val in y_slider.val:
        plot_hline(val)
    colors = bwr(diff_dists(y_slider.val))
    ax.bar(data.columns,
           data.loc['mean'],
           yerr=error, align='center',
           alpha=1,
           ecolor='black',
           capsize=10,
           color= colors)
    ax.set_xlim(1991, 1996)
    sax.sharey(ax)
    fig.canvas.draw_idle()


y_slider.on_changed(update)
ax.set_xticks(data.columns)
fig.tight_layout()
cbar = plt.colorbar(sc_map,cax=cax)
plt.subplots_adjust(right=0.9)
cbar.ax.set_ylabel('Probability of being inside range')

