import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm 
import matplotlib.colors as c
from matplotlib.widgets import Slider, Button

# Get data
gdp = pd.read_csv('data/gdpdata.csv',skiprows=2, header=1)
sui = pd.read_csv('data/suicidedata.csv',skiprows=1, header=0)
sui.columns = sui.columns.str.replace(' ', '')
sui = sui.rename(columns={'Country': 'Country Name'})
urban = pd.read_csv('data/urbanpop.csv',skiprows=2, header=1)


# Preprocess each table
cells = ['Country Name']
years = [ str(x) for x in range(2005,2020)]
cells += years
gdpdf = gdp[cells].dropna()
urbandf = urban[cells].dropna()
gdpds = gdpdf.set_index('Country Name').stack()
urbands = urbandf.set_index('Country Name').stack()

suidf = sui[sui['Sex']==' Both sexes']
suidf = suidf[cells]
pattern = r"\[.*\]"
suidf.replace(to_replace=pattern, value='', inplace=True, regex=True)
suidf[years] = suidf[years].apply(lambda x: x.str.strip()).astype('float')
suidf = suidf.reset_index(drop=True)
suids = suidf.set_index('Country Name').stack()

# Join tables for visualization
data = pd.concat([suids,gdpds,urbands], axis=1)
data = data.reset_index().dropna()
data.columns = ['Country', 'Year', 'Suicides', 'GDP', '%Urban' ]
data =data.set_index(['Country','Year']).reset_index()


# Construct visualization
def get_data(year):
    return data[data['Year']==str(year)]

ds = get_data(2005)
cmap='rainbow'
fig,(ax,sax) = plt.subplots(2,1,gridspec_kw={"height_ratios":[1,0.05]})
fig.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.4)
cm.get_cmap(name=cmap, lut=None)
norm = c.Normalize(vmin=ds['GDP'].min(), vmax=60000, clip=True)

y_slider = Slider(
    ax=sax,
    label='Years',
    valmin=2005,
    valmax=2018,
    valinit=(2010),
    valstep=1
)
cbar = fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)

def draw_fig(year, cmap):
    ax.set_xlim([5,105])
    ax.set_ylim([0,85])
    ax.set_xlim([5,105])
    ax.set_ylim([0,85])
    ax.set_xlabel('Percentage of Population in Urban Areas')
    ax.set_ylabel('Suicides per 10000 habitants')
    cbar.ax.set_ylabel('GDP Per capita (USD)')
    ds = get_data(year)
    norm = c.Normalize(vmin=ds['GDP'].min(), vmax=60000, clip=True)
    cbar.update_normal(cm.ScalarMappable(norm=norm, cmap=cmap))
    ax.scatter(ds['%Urban'], ds['Suicides'], c=ds['GDP'],cmap=cmap, alpha=0.8)
    fig.suptitle('Suicide rate vs Percentage of urban population ({})'.format(year))
    return None

def update(val):
    ax.cla()
    draw_fig(val,cmap)
    fig.canvas.draw_idle()
    return None

draw_fig(2010,cmap)
y_slider.on_changed(update)
plt.grid()
plt.show()
